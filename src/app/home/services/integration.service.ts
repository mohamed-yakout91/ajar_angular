import { HttpClient } from '@angular/common/http';

import { Injectable, Inject } from '@angular/core';
// import { Observable } from 'rxjs/Observable';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
//
// import 'rxjs/add/operator/map';
import { map } from 'rxjs/operators';
// import * as moment from 'moment';

@Injectable()
export class IntegrationService {
  public api_url = "http://localhost:2400/";

  constructor(public _http: HttpClient) {
  }

  public getBankInfo(iban) {
    return this._http.get(this.api_url + 'api/v1/bank/' + iban, {}).pipe(map((res) => res));
  }

  public getAjarBalance() {
    return this._http.get(this.api_url + 'api/v1/balance', {}).pipe(map((res) => res));
  }

  public transferByIban(iban, amount, currency) {
      let postData = {
        "amount": amount,
        "currency": currency
      };
      return this._http.post(this.api_url + 'api/v1/transfer/' + iban, postData, {}).pipe(map((res) => res));
  }
}
