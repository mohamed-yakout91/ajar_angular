import {
  Component,
  OnInit
} from '@angular/core';

import { AppState } from '../app.service';
import { IntegrationService } from './services/integration.service';
import { Title } from './title';
import { XLargeDirective } from './x-large';

// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { BehaviorSubject } from "rxjs";

@Component({
  /**
   * The selector is what angular internally uses
   * for `document.querySelectorAll(selector)` in our index.html
   * where, in this case, selector is the string 'home'.
   */
  selector: 'home',  // <home></home>
  /**
   * We need to tell Angular's Dependency Injection which providers are in our app.
   */
  providers: [
    Title
  ],
  /**
   * Our list of styles in our component. We may add more to compose many styles together.
   */
  styleUrls: [ './home.component.css' ],
  /**
   * Every Angular template is first compiled by the browser before Angular runs it's compiler.
   */
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  /**
   * Set our default values AE070331234567890123456
   */
  public localState = { value: '' };
  // public ibanState = { iban: 'AE070331234567890123456', amount: 1000.0, currency: 'AED' };
  public ibanState = { iban: '', amount: 0.0, currency: 'AED' };

  public bankInfo$: BehaviorSubject<any> = new BehaviorSubject(null);
  public ajarBalance$: BehaviorSubject<any> = new BehaviorSubject(null);
  /**
   * TypeScript public modifiers
   */
  constructor(
    public appState: AppState,
    public title: Title,
    public integrationService: IntegrationService
  ) {}

  public ngOnInit() {
    console.log('hello `Home` component');
    /**
     * this.title.getData().subscribe(data => this.data = data);
     */
     this.resetIbanState();
  }

  public resetIbanState = function() {
    this.ibanState = { iban: '', amount: 0.0, currency: 'AED' };
  }

  public submitState(value: string) {
    console.log('submitState', value);
    this.appState.set('value', value);
    this.localState.value = '';
  }

  public submitTransfer() {
    this.integrationService.transferByIban(this.ibanState.iban, this.ibanState.amount, this.ibanState.currency).subscribe((res) => {
      console.log(res);
      this.resetIbanState();
    });
  }
  public getBankInfo() {
    this.integrationService.getBankInfo(this.ibanState.iban).subscribe((res) => {
      console.log(res);
      this.bankInfo$.next(res["data"]);
    });
  }

  public getAjarBalance() {
    this.integrationService.getAjarBalance().subscribe((res) => {
      console.log(res);
      this.ajarBalance$.next(res["data"])
    });

  }
}
